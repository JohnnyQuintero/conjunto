/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Conjunto;

/**
 *
 * @author MADARME
 */
public class TestNuevoConjunto {

    public static void main(String[] args) {
        try {

            Conjunto<Integer> c1 = new Conjunto(6);
            Conjunto<Integer> c2 = new Conjunto(9);
            Conjunto<Integer> c3 = new Conjunto(30);
            //probar todos los métodos públicos(imprimir)
            //Recomendación: Realicen método para crear cada una de las pruebas.
            //LLenando conjuntos
            for (int i = 1; i <= 3; i++) {
                c1.adicionarElemento(i);
                c3.adicionarElemento(i);
            }
            for (int i = 4; i <= 6; i++) {
                c1.adicionarElemento(i);
                c3.adicionarElemento(i);
            }
            for (int i = 0, aux = 0; i < 5; i++) {
                aux += 10;
                c2.adicionarElemento(aux);
                c3.adicionarElemento(aux);
            }
            for (int i = 5, aux = 1; i < 9; i++) {
                aux += i;
                c2.adicionarElemento(aux + 10);
                c3.adicionarElemento(aux + 10);
            }
            for (int i = 0, aux = 100; i < 15; i++) {
                aux += i;
                c3.adicionarElemento(aux);
            }

            System.out.println("test");
            System.out.println("Conjunto Normal");
            imprimir(c1);
            imprimir(c2);
            System.out.println("Remover");
            c1.remover(4);
            c2.remover(10);
            imprimir(c1);
            imprimir(c2);
            test(c2, c3);
            //test(c1,c2);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static <T> void test(Conjunto<T> n1, Conjunto<T> n2) {
        System.out.println("Conjunto Despues de remover");
        imprimir(n1);
        imprimir(n2);
        System.out.println("Conjunto Ordenado Burbuja");
        ordenarBubuja(n1);
        ordenarBubuja(n2);
        imprimir(n1);
        imprimir(n2);
        System.out.println("Conjunto Ordenado Seleccion");
        ordenarSeleccion(n1);
        ordenarSeleccion(n2);
        imprimir(n1);
        imprimir(n2);
        //No se puede concatenar normal y resctrictivo usar a la vez
        //Se comenta uno y se descomenta el otro :)
//            System.out.println("Concatenar");
//            concatenar(n1,n2);
//            imprimir(n1);
        System.out.println("Concatenar Resctrictivo");
        concatenarRestrictivo(n1, n2);
        imprimir(n1);

    }

    public static <T> void imprimir(T objetoDesconocido) {
        System.out.println(objetoDesconocido.toString());
    }

    public static <T> void ordenarSeleccion(Conjunto<T> objetoDesconocido) {
        objetoDesconocido.ordenar();
    }

    public static <T> void ordenarBubuja(Conjunto<T> objetoDesconocido) {
        objetoDesconocido.ordenarBurbuja();
    }

    public static <T> void concatenar(Conjunto<T> objetoDesconocido, Conjunto<T> nuevo) {
        objetoDesconocido.concatenar(nuevo);
    }

    public static <T> void concatenarRestrictivo(Conjunto<T> objetoDesconocido, Conjunto<T> nuevo) {
        objetoDesconocido.concatenarRestrictivo(nuevo);
    }
}
